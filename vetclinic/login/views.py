import json

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.template.context_processors import csrf
from django.views.decorators.csrf import csrf_protect


@csrf_protect
def index(request):
    c = {}
    c.update(csrf(request))

    return render(request, 'index.html', c)


@csrf_protect
def login_vetclinic(request):
    c = {}
    c.update(csrf(request))

    username = request.POST.get('username')
    password = request.POST.get('password')

    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        response = {'status': 'SUCCESS', 'code': 100}
        return HttpResponse(json.dumps(response))
    else:
        response = {'status': 'FAIL', 'code': 101}
        return HttpResponse(json.dumps(response))


@csrf_protect
def register_vetclinic(request):
    c = {}
    c.update(csrf(request))

    username = request.POST.get('username')
    password = request.POST.get('password')
    email = request.POST.get('email')

    try:
        user = User.objects.create_user(username, email, password)
        user.save()
        response = {'status': 'USER SAVED', 'code': 100}
    except Exception:
        response = {'status': 'REGISTRATION FAILED', 'code': 101}

    return HttpResponse(json.dumps(response))
